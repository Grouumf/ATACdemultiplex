module gitlab.com/Grouumf/ATACdemultiplex/ATACSimUtils

go 1.15

require (
	github.com/valyala/fastrand v1.0.0
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210802220835-4b20ff939e6b
)

/* Suite of functions for generic matrix manipulation */


package main


import(
	"bufio"
	// "log"
	"flag"
	"os"
	"io"
	utils "gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils"
	// "github.com/biogo/store/interval"
	"fmt"
	"strings"
	"strconv"
	"time"
	"bytes"
	"sync"
	"path"
	// "sort"
	// "math"
)

type matrixFormat string


/*INPUTMAT input matrix name (input) */
var INPUTMAT utils.Filename

/*INPUTMAT2 input matrix name (input) */
var INPUTMAT2 utils.Filename

/*XGI row index for input mat */
var XGI utils.Filename

/*YGI column index for input mat */
var YGI utils.Filename

/*XGI2 row index for input mat 2 */
var XGI2 utils.Filename

/*YGI2 column index for input mat 2 */
var YGI2 utils.Filename

/*XGISUBSET Subset rows using this new row index */
var XGISUBSET utils.Filename

/*YGISUBSET Subset columns using this new col index */
var YGISUBSET utils.Filename

/*FILENAMEOUT  output file name output */
var FILENAMEOUT string

/*SEP  separator for input matrix */
var SEP string

/*OUTSEP  separator for input matrix */
var OUTSEP string

/*CONVERT Convert matrix format */
var CONVERT bool

/*JOIN Merge matrices based on inner joined features  */
var JOIN bool

/*SUBSET Subset matrix  */
var SUBSET bool

/*CONVERTTO Format to convert */
var CONVERTTO string

/*INTYPE input matrix format */
var INTYPE string

/*BUFFSIZE Buffer size for reader */
var BUFFSIZE int

/*HEADER Use header when reading matrix */
var HEADER bool

/*RMDUPPLICATES Remove dupplicates entries for rows and columns */
var RMDUPPLICATES bool

/*TRANSPOSE Transpose matrix */
var TRANSPOSE bool

/*SHIFTHEADER Shift header to right (wrongly formatted header) */
var SHIFTHEADER bool

/*INDEXFIRSTCOL Use first col as index */
var INDEXFIRSTCOL bool

/*WRITEINDEXES Write xgi ygi indexes */
var WRITEINDEXES bool

/*XGIINDEX index for row*/
var XGIINDEX []string

/*XGIMAP index for row*/
var XGIMAP map[string]int

/*YGIMAP index for row*/
var YGIMAP map[string]int

/*YGIINDEX index for column*/
var YGIINDEX []string

/*XGISUBSETMAP subset mapping index */
var XGISUBSETMAP map[string]int

/*YGISUBSETMAP subset mapping index */
var YGISUBSETMAP map[string]int

/*MUTEX main mutex */
var MUTEX sync.Mutex

/*MUTEX2 main mutex */
var MUTEX2 sync.Mutex

/*THREADNB number of threads for reading the bam file */
var THREADNB int

/*XDIM effective xdim used*/
var XDIM int

/*YDIM effective ydim used*/
var YDIM int

/*FLOATMATRIX cell x features float */
var FLOATMATRIX []map[int]float64

const (
	// Matrix format
	coo matrixFormat = "coo"
	dense matrixFormat = "dense"
	npz matrixFormat = "npz"
)

/*MATRIXFORMATS possible options for matrix format */
var MATRIXFORMATS = [...]matrixFormat{coo, dense}


func (t matrixFormat) isValid() matrixFormat {
	found := false

	for _, typ := range MATRIXFORMATS {
		if typ == t {
			found = true
		}
	}

	if !found {
		panic(fmt.Sprintf("#### Error matrix type not valid! %s\n", t))
	}

	return t
}

func main () {

		flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `
#################### Generic module to manipulate matrix files (in construction) ########################

====> Convert dense matrix to sparse <====
USAGE:

# Can be used to remove dupplicates
MatUtils -subset -in <matrix file> -out <output file> -xgi <index row> -ygi <index col> (-subset_xgi <Xgi file> -subset_ygi <Ygi file> -type <mat type> -rm_dupplicates)

MatUtils -convert -in <matrix file> -out <matrix file> (-type <str> -format <str> -subset_xgi <file> -subset_ygi <file> -sep <string> -out_sep <string> -header -write_indexes -index_first_col -shift_header -buffer_size <int> -transpose -rm_dupplicates -threads <int>)

====> Join 2 sparse matrices based on their common set of features <====
MatUtils -join -in <matrix file> -in2 <matrix file> -ygi <ygi file> -ygi2 <ygi file>  -out <matrix file>
            (optional: -xgi <xgi file> -xgi2 <xgi file>)

`)
			flag.PrintDefaults()
		}

	flag.BoolVar(&CONVERT, "convert", false,  "Convert matrix format ")
	flag.BoolVar(&SUBSET, "subset", false,  "Subset matrix ")
	flag.BoolVar(&JOIN, "join", false,  "Convert matrix format ")
	flag.BoolVar(&TRANSPOSE, "transpose", false,  "Transpose matrix")
	flag.BoolVar(&RMDUPPLICATES, "rm_dupplicates", false,  "Remove dupplicates entries for rows and columns ")
	flag.Var(&INPUTMAT, "in", "Input matrix file")
	flag.Var(&INPUTMAT2, "in2", " Second input matrix file (for join analysis)")
	flag.Var(&XGI, "xgi", "row index for input mat")
	flag.Var(&YGI, "ygi", "column index for input mat")
	flag.Var(&XGI2, "xgi2", "Second row index for input mat")
	flag.Var(&YGI2, "ygi2", "Second column index for input mat")
	flag.Var(&XGISUBSET, "subset_xgi", "Subset rows with a new index file")
	flag.Var(&YGISUBSET, "subset_ygi", "Subset columns with a new index file")
	flag.StringVar(&FILENAMEOUT, "out", "", "name of the output file")
	flag.StringVar(&CONVERTTO, "format", "coo", "Format to convert the input matrix")
	flag.StringVar(&INTYPE, "type", "dense", "Input matrix type")
	flag.StringVar(&SEP, "sep", "\t", "Separator for input matrix")
	flag.StringVar(&OUTSEP, "out_sep", "\t", "Separator for output matrix")
	flag.BoolVar(&HEADER, "header", false,  "Use first line as header when reading matrix ")
	flag.BoolVar(&WRITEINDEXES, "write_indexes", false,  "Write xgi (row) / ygi (col) indexes ")
	flag.BoolVar(&INDEXFIRSTCOL, "index_first_col", false,  "Use first col as row index")
	flag.BoolVar(&SHIFTHEADER, "shift_header", false,  "Shift header to the right (wrongly formatted header")
	flag.IntVar(&BUFFSIZE, "buffer_size", 1000000, "Buffer size when reading matrix. In case of very large matrix incrase it")
	flag.IntVar(&THREADNB, "threads", 4, "threads concurrency")

	flag.Parse()

	switch {
	case SUBSET:
		subsetMat()
	case CONVERT:
		convertMat()
	case JOIN:
		joinMatCoo()
	default:
		flag.PrintDefaults()
		panic(fmt.Sprintf("#### Error wrong usage !\n"))
	}
}

func subsetMat() {
	if FILENAMEOUT == "" {
		panic(fmt.Sprintf("#### Error -out must be defined! \n"))
	}

	intype := matrixFormat(INTYPE).isValid()

	XGIMAP = make(map[string]int)
	var cleanindexX, cleanindexY map[string]int

	XGIINDEX, XGIMAP, cleanindexX  = utils.LoadIndexMarkDupplicates(XGI, SEP)
	YGIINDEX, YGIMAP, cleanindexY = utils.LoadIndexMarkDupplicates(YGI, SEP)

	fmt.Printf("Number of dupplicate for row: %d\n", len(XGIINDEX) - len(cleanindexX))
	fmt.Printf("Number of dupplicate for col: %d\n", len(YGIINDEX) - len(cleanindexY))

	if XGISUBSET != "" {
		_, XGISUBSETMAP = utils.LoadIndex(XGISUBSET, SEP)
	}

	if YGISUBSET != "" {
		_, YGISUBSETMAP = utils.LoadIndex(YGISUBSET, SEP)
	}

	var rmXgiSubset, rmYgiSubset bool

	if RMDUPPLICATES {
		if len(XGISUBSET) == 0 && len(XGIINDEX) - len(cleanindexX) > 0 {
			XGISUBSETMAP = cleanindexX
		} else {
			rmXgiSubset = true
		}

		if len(YGISUBSET) == 0 && len(YGIINDEX) - len(cleanindexY) > 0 {
			YGISUBSETMAP = cleanindexY
		} else {
			rmYgiSubset = true
		}
	}

	tStart := time.Now()


	switch {
	case intype == coo:
		subsetCoo()
	default:
		panic(fmt.Sprintf("#### Subsetting %s currently not supported\n", intype))
	}

	tDiff := time.Since(tStart)
	fmt.Printf("Subsettting matrix done in: %f s\n", tDiff.Seconds())
	fmt.Printf("Matrix written: %s\n", FILENAMEOUT)

	if RMDUPPLICATES {
		if rmXgiSubset {
			XGISUBSETMAP = make(map[string]int)
		}

		if rmYgiSubset {
			YGISUBSETMAP = make(map[string]int)
		}

		writeIndexes()
	}
}

func subsetCoo() {
	buffers := make([]bytes.Buffer, THREADNB)
	guard := make(chan int, THREADNB)

	for i:=0; i < THREADNB ;i++ {
		guard <- i
		buffers[i] = bytes.Buffer{}
	}

	waiting := sync.WaitGroup{}

	reader, file := INPUTMAT.ReturnReader(0)
	writer := utils.ReturnWriter(FILENAMEOUT)

	defer utils.CloseFile(writer)
	defer utils.CloseFile(file)

	var count, buffSize int

	isSubsetXgi := len(XGISUBSET) > 0
	isSubsetYgi := len(YGISUBSET) > 0

	currentThread := <- guard
	var line string
	var split []string
	var indexRow, indexCol int
	var err1, err2 error
	var isInside bool

	buffer := &(buffers[currentThread])

	maxBuffSize := 5000

	for reader.Scan() {
		count++
		line = reader.Text()
		split = strings.Split(line, "\t")

		indexRow, err1 = strconv.Atoi(split[0])
		indexCol, err2 = strconv.Atoi(split[1])

		if err1 != nil || err2 != nil  {
			panic(fmt.Sprintf("Error when parsing line nb (%d): %s \n", count, line))
		}

		if isSubsetXgi {
			if indexRow, isInside = XGISUBSETMAP[XGIINDEX[indexRow]];!isInside {
				continue
			}
		}

		if isSubsetYgi {
			if indexCol, isInside = YGISUBSETMAP[YGIINDEX[indexCol]];!isInside {
				continue
			}
		}

		(*buffer).WriteString(strconv.Itoa(indexRow))
		(*buffer).WriteRune('\t')
		(*buffer).WriteString(strconv.Itoa(indexCol))
		(*buffer).WriteRune('\t')
		(*buffer).WriteString(split[2])
		(*buffer).WriteRune('\n')

		buffSize++

		if buffSize > maxBuffSize {
			waiting.Add(1)
			buffSize = 0
			go writeBuffer(&writer, buffer, &waiting, currentThread, guard)
			currentThread = <- guard
			buffer = &(buffers[currentThread])
		}
	}

	waiting.Add(1)
	go writeBuffer(&writer, buffer, &waiting, currentThread, guard)
	waiting.Wait()
}

func writeBuffer(writer *io.WriteCloser, buffer * bytes.Buffer, waiting *sync.WaitGroup, currentThread int, guard chan int) {
	defer waiting.Done()
	MUTEX.Lock()
	_, err := (*writer).Write(buffer.Bytes())
	utils.Check(err)
	buffer.Reset()
	MUTEX.Unlock()
	guard <- currentThread
}

func findFeaturesIntersection(ygimap1, ygimap2 map[string]int) (commonIndex []string, commonCols map[string]int){
	commonCols = make(map[string]int)
	index := 0
	for col := range ygimap1 {
		if _, isInside := ygimap2[col];isInside {
			if len(YGISUBSETMAP) > 0 {
				if _, isInside := YGISUBSETMAP[col];!isInside {
					continue
				}
			}

			commonCols[col] = index
			commonIndex = append(commonIndex, col)
			index++
		}
	}

	return commonIndex, commonCols
}


func loadXgisForJoin() (xgiindex1, xgiindex2 []string) {

	xgiindex1, xgiindex2 = []string{}, []string{}

	if XGISUBSET != "" || XGI != "" || XGI2 != ""  {
		if XGI == "" {
			panic(fmt.Sprintf("-xgi option must be filled when -subset_xgi or -xgi2 is provided\n", ))
		}

		if XGI2 == "" {
			panic(fmt.Sprintf("-xgi2 option must be filled when -subset_xgi or -xgi is provided\n", ))
		}

		xgiindex1, _ = utils.LoadIndex(XGI, SEP)
		xgiindex2, _ = utils.LoadIndex(XGI2, SEP)

		if XGISUBSET != "" {
			_, XGISUBSETMAP = utils.LoadIndex(XGISUBSET, SEP)
		}

		XGIINDEX = append(xgiindex1, xgiindex2...)
	}

	return xgiindex1, xgiindex2
}


func joinMatCoo() {
	if FILENAMEOUT == "" {
		panic(fmt.Sprintf("#### Error -out must be defined! \n"))
	}

	tStart := time.Now()

	ygiindex1, ygimap1 := utils.LoadIndex(YGI, SEP)
	ygiindex2, ygimap2 := utils.LoadIndex(YGI2, SEP)

	xgiindex1, xgiindex2 := loadXgisForJoin()

	if YGISUBSET != "" {
		_, YGISUBSETMAP = utils.LoadIndex(YGISUBSET, SEP)
	}

	commonIndex, commonCols := findFeaturesIntersection(ygimap1, ygimap2)
	fmt.Printf("Number of common features: %d\n", len(commonCols))
	YGISUBSETMAP = commonCols
	YGIINDEX = commonIndex

	writer := utils.ReturnWriter(FILENAMEOUT)
	defer utils.CloseFile(writer)

	reader1, file := returnReader(INPUTMAT)

	writeCooToCoo(
		reader1, &writer,
		xgiindex1, ygiindex1, 0)
	utils.CloseFile(file)
	fmt.Printf("Number of row first mat: %d\n", XDIM)

	reader2, file2 := returnReader(INPUTMAT2)
	defer utils.CloseFile(file2)

	writeCooToCoo(
		reader2, &writer,
		xgiindex2, ygiindex2, XDIM)

	writeDummyXgiIndex(XDIM)

	fmt.Printf("Matrix shape: %d rows x %d cols\n", XDIM, len(YGISUBSETMAP))
	fmt.Printf("Matrix file written:%s\n", FILENAMEOUT)
	tDiff := time.Since(tStart)
	fmt.Printf("Done in: %f s\n", tDiff.Seconds())

	writeIndexes()
}

func writeDummyXgiIndex(length int) {
	if len(XGIINDEX) == 0 {
		XGIINDEX = make([]string, length)

		for i:=0;i<length;i++{
			XGIINDEX[i] =  strconv.Itoa(i)
		}
	}
}


func returnReader(inputfile utils.Filename) (reader *bufio.Scanner, file *os.File) {
	reader, file = inputfile.ReturnReader(0)
	buffer := make([]byte, BUFFSIZE)

	reader.Buffer(buffer, BUFFSIZE)

	return reader, file
}


func writeCooToCoo(reader * bufio.Scanner, writer *io.WriteCloser, xgiindex, ygiindex []string,  startingRow int) {
	guard := make(chan bool, THREADNB)

	for i:=0;i<THREADNB;i++ {
		guard <- true
	}

	indexLine := 0
	waiting := sync.WaitGroup{}

	lines := make([]string, 80000)
	nbRow := 0

	for reader.Scan() {
		if indexLine >= 80000 {
			<-guard
			go writeCooToCooOneThread(
				lines, writer, &xgiindex,
				&ygiindex, guard, &waiting, startingRow)
			lines = make([]string, 80000)
			indexLine = 0
		}

		lines[indexLine] = reader.Text()
		indexLine++
		nbRow++
	}

	<-guard
	go writeCooToCooOneThread(
		lines[:indexLine], writer, &xgiindex,
		&ygiindex, guard, &waiting, startingRow)

	waiting.Wait()
}

func writeCooToCooOneThread(lines []string, writer *io.WriteCloser, xgiindex, ygiindex *[]string, guard chan bool, waiting *sync.WaitGroup, startingRow int) {
	waiting.Add(1)
	defer waiting.Done()

	var indexRow, indexCol int
	var err1, err2, err3 error

	var xgi, ygi string
	var isInside bool

	buffer := bytes.Buffer{}

	var rowStr, colStr string

	maxRow := 0

	subsetXgi := len(XGISUBSETMAP) > 0
	nbFeatures := len(*ygiindex)
	var split []string
	var line string
	xgiindexSet := len(*xgiindex) > 0
	xgiindexLength := len(*xgiindex)
	xdim := 0

	for _, line = range lines {
		split = strings.Split(line, SEP)

		indexRow, err1 = strconv.Atoi(split[0])
		indexCol, err2 = strconv.Atoi(split[1])
		_, err3 = strconv.ParseFloat(split[2], 32)

		if err1 != nil || err2 != nil || err3 != nil {
			panic(fmt.Sprintf("Error when parsing line: %s \n", line))
		}

		if indexCol >= nbFeatures {
			panic(fmt.Sprintf("Error! Index column (%d) higher than maximum number of features: %d for line: %s\n", indexCol, nbFeatures, split))
		}

		if indexRow > maxRow {
			maxRow = indexRow
		}

		ygi = (*ygiindex)[indexCol]

		if indexCol, isInside = YGISUBSETMAP[ygi];!isInside {
			continue
		}

		if xgiindexSet {
			if indexRow >= xgiindexLength {
				panic(fmt.Sprintf("Error! Index row (%d) higher than the input number of rows: %d for line: %s\n", indexRow, xgiindexLength, split))
			}

		}

		if subsetXgi {
			xgi = (*xgiindex)[indexRow]

			if indexRow, isInside = XGISUBSETMAP[xgi];!isInside {
				continue
			}
		}

		rowStr = strconv.Itoa(indexRow + startingRow)
		colStr = strconv.Itoa(indexCol)

		if TRANSPOSE {
			rowStr, colStr = colStr, rowStr
		}

		buffer.WriteString(rowStr)
		buffer.WriteString(OUTSEP)
		buffer.WriteString(colStr)
		buffer.WriteString(OUTSEP)
		buffer.WriteString(split[2])
		buffer.WriteRune('\n')
	}

	if xgiindexSet {
		xdim = xgiindexLength + startingRow
	} else {
		xdim = maxRow + 1 + startingRow
	}

	MUTEX.Lock()

	if xdim > XDIM {
		XDIM = xdim
	}

	_, err1 = (*writer).Write(buffer.Bytes())
	buffer.Reset()
	utils.Check(err1)
	MUTEX.Unlock()

	guard <- true
}


func convertMat() {
	if FILENAMEOUT == "" {
		panic(fmt.Sprintf("#### Error -out must be defined! \n"))
	}
	intype := matrixFormat(INTYPE).isValid()
	outtype := matrixFormat(CONVERTTO).isValid()

	XGIMAP = make(map[string]int)

	tStart := time.Now()

	switch {
	case XGI != "" && RMDUPPLICATES:
		XGIINDEX, XGIMAP, XGISUBSETMAP  = utils.LoadIndexMarkDupplicates(XGI, SEP)
		fmt.Printf("#### %d dupplicated  Row id found \n", len(XGIINDEX) - len(XGISUBSETMAP))
	case XGI != "":
		XGIINDEX, XGIMAP = utils.LoadIndex(XGI, SEP)
	}

	switch {
	case YGI != "" && RMDUPPLICATES:
		YGIINDEX, _, YGISUBSETMAP  = utils.LoadIndexMarkDupplicates(YGI, SEP)
		fmt.Printf("#### %d dupplicated  col id found \n", len(YGIINDEX) - len(YGISUBSETMAP))
	case YGI != "":
		YGIINDEX, _ = utils.LoadIndex(YGI, SEP)
	}

	if XGISUBSET != "" {
		_, XGISUBSETMAP = utils.LoadIndex(XGISUBSET, SEP)
	}

	if YGISUBSET != "" {
		_, YGISUBSETMAP = utils.LoadIndex(YGISUBSET, SEP)
	}

	switch {

	case intype == dense && outtype == coo:
		convertDenseToCoo()
	case intype == coo && outtype == dense:
		convertCooToDense()

	default:
		panic(fmt.Sprintf("#### Converting %s to %s currently not supported\n", intype, outtype))

	}

	tDiff := time.Since(tStart)
	fmt.Printf("Converting matrix done in: %f s\n", tDiff.Seconds())

	if WRITEINDEXES {
		writeIndexes()
	}
}

func convertCooToDense() {
	WRITEINDEXES = false

	tStart := time.Now()
	loadCootoFloatMatrix()
	tDiff := time.Since(tStart)
	fmt.Printf("Sparse matrix loaded in: %f s\n", tDiff.Seconds())

	var err error

	writer := utils.ReturnWriter(FILENAMEOUT)
	defer utils.CloseFile(writer)

	writer.Write([]byte(OUTSEP))
	header := strings.Join(YGIINDEX, OUTSEP)

	_, err = writer.Write([]byte(header))
	utils.Check(err)
	writer.Write([]byte("\n"))

	guard := make(chan bool, THREADNB)

	for i:=0;i<THREADNB;i++ {
		guard <- true
	}

	waiting := sync.WaitGroup{}

	for indexRow := range FLOATMATRIX {
		<-guard
		waiting.Add(1)
		go writeRowToDenseOneThread(indexRow, &writer, &waiting, guard)
	}

	fmt.Printf("Dense matrix file created: %s\n", FILENAMEOUT)
	waiting.Wait()
}

func writeRowToDenseOneThread(indexRow int, writer * io.WriteCloser, waiting * sync.WaitGroup, guard chan bool) {
	defer waiting.Done()

	buffer := bytes.Buffer{}

	xgiStr := XGIINDEX[indexRow]

	buffer.WriteString(xgiStr)

	var val float64
	var isInside bool
	var err error

	for indexCol := range YGIINDEX {
		buffer.WriteString(OUTSEP)
		if val, isInside = FLOATMATRIX[indexRow][indexCol];!isInside {
			buffer.WriteRune('0')
		} else {
			buffer.WriteString(strconv.FormatFloat(val, 'E', 5, 64))
		}
	}

	buffer.WriteRune('\n')

	MUTEX.Lock()
	_, err = (*writer).Write(buffer.Bytes())
	utils.Check(err)
	MUTEX.Unlock()

	guard <- true
}

func writeIndexes() {
	ext := path.Ext(FILENAMEOUT)
	fxgi := fmt.Sprintf(
		"%s.index.xgi", FILENAMEOUT[:len(FILENAMEOUT) - len(ext)])
	fygi := fmt.Sprintf(
		"%s.index.ygi", FILENAMEOUT[:len(FILENAMEOUT) - len(ext)])

	if TRANSPOSE {
		fxgi, fygi = fygi, fxgi
	}

	wxgi := utils.ReturnWriter(fxgi)
	wygi := utils.ReturnWriter(fygi)

	var useYgiSubset, useXgiSubset bool
	var err error
	// var index int
	var isInside bool

	if len(XGISUBSETMAP) > 0 {
		useXgiSubset = true
	}

	if len(YGISUBSETMAP) > 0 {
		useYgiSubset = true
	}

	for _, xgi := range XGIINDEX {
		if useXgiSubset {
			if _, isInside = XGISUBSETMAP[xgi];!isInside{
				continue
			}
		}

		_, err = wxgi.Write([]byte(fmt.Sprintf("%s\n", xgi)))
	}

	utils.Check(err)
	utils.CloseFile(wxgi)
	fmt.Printf("XGI index file written: %s\n", fxgi)

	for _, ygi := range YGIINDEX {
		if useYgiSubset {
			if _,isInside = YGISUBSETMAP[ygi];!isInside{
				continue
			}
		}

		wygi.Write([]byte(fmt.Sprintf("%s\n", ygi)))
	}

	utils.Check(err)
	utils.CloseFile(wygi)
	fmt.Printf("YGI index file written: %s\n", fygi)
}


func loadCootoFloatMatrix() {
	reader, file := INPUTMAT.ReturnReader(0)
	defer file.Close()

	var split []string
	var posx, posy int
	var val float64
	var err error
	var xgiStr, ygiStr string
	var isInside bool

	subsetXgi := len(XGISUBSETMAP) > 0
	subsetYgi := len(YGISUBSETMAP) > 0
	xgiindexsubset := make([]string, len(XGISUBSETMAP))
	ygiindexsubset := make([]string, len(YGISUBSETMAP))

	if len(XGIINDEX) == 0 {
		panic(fmt.Sprintf("#### Error: -xgi must be passed for coo to dense conversion\n" ))
	}

	if TRANSPOSE {
		if subsetYgi {
			FLOATMATRIX = make([]map[int]float64, len(YGISUBSETMAP))
		} else {
			FLOATMATRIX = make([]map[int]float64, len(YGIINDEX))
		}

	} else {
		if subsetXgi {
			FLOATMATRIX = make([]map[int]float64, len(XGISUBSETMAP))
		} else {
			FLOATMATRIX = make([]map[int]float64, len(XGIINDEX))
		}
	}

	for reader.Scan() {
		split = strings.Split(reader.Text(), SEP)

		posx, err = strconv.Atoi(split[0])
		utils.Check(err)

		if subsetXgi {
			xgiStr = XGIINDEX[posx]

			if posx, isInside = XGISUBSETMAP[xgiStr];!isInside {
				continue
			}

			xgiindexsubset[posx] = xgiStr
		}

		posy, err = strconv.Atoi(split[1])
		utils.Check(err)

		if subsetYgi {
			ygiStr = YGIINDEX[posy]

			if posy, isInside = YGISUBSETMAP[ygiStr];!isInside {
				continue
			}

			ygiindexsubset[posy] = ygiStr
		}

		val, err = strconv.ParseFloat(split[2], 32)
		utils.Check(err)

		if TRANSPOSE {
			posx, posy = posy, posx
		}

		if len(FLOATMATRIX[posx]) == 0 {
			FLOATMATRIX[posx] = make(map[int]float64)
		}

		FLOATMATRIX[posx][posy] = val
	}

	if subsetXgi {
		XGIINDEX = xgiindexsubset
	}

	if subsetYgi {
		YGIINDEX = ygiindexsubset
	}

	if TRANSPOSE {
		XGIINDEX, YGIINDEX = YGIINDEX, XGIINDEX
	}
}


func convertDenseToCoo() {
	reader, file := INPUTMAT.ReturnReader(0)
	lineBuffer := make([]byte, BUFFSIZE)

	reader.Buffer(lineBuffer, BUFFSIZE)

	if HEADER {
		readFirstLineAsHeader(reader)
	}

	writer := utils.ReturnWriter(FILENAMEOUT)
	defer utils.CloseFile(writer)

	guard := make(chan bool, THREADNB)

	if len(XGISUBSETMAP) == 0 {
		XGISUBSETMAP = make(map[string]int)
	}

	for i:=0;i<THREADNB;i++ {
		guard <- true
	}

	indexRow := 0
	waiting := sync.WaitGroup{}

	for reader.Scan() {
		utils.Check(reader.Err())
		<-guard

		go processOneLineFromDenseMat(
			reader.Text(),
			indexRow,
			&writer,
			&waiting,
			guard)
		indexRow++
	}

	waiting.Wait()
	defer utils.CloseFile(file)

	if INDEXFIRSTCOL || XGI == "" {
		XGIINDEX = make([]string, len(XGIMAP))

		for xgi, index := range XGIMAP {
			XGIINDEX[index] = xgi
		}
	}

	if TRANSPOSE {
		XDIM, YDIM = YDIM, XDIM
	}

	fmt.Printf("#### Shape: input matrix: rows: %d x cols: %d \n", len(XGIINDEX), len(YGIINDEX))
	fmt.Printf("#### Shape: output matrix: rows: %d x cols: %d \n", XDIM, YDIM)
	fmt.Printf("#### output matrix written: %s\n", FILENAMEOUT)
}


func processOneLineFromDenseMat(line string, indexRow int, writer * io.WriteCloser, waiting * sync.WaitGroup, guard chan bool) {
	waiting.Add(1)
	defer waiting.Done()
	var xgi, ygi, valstr, index1, index2 string
	var err error
	var val float64
	var indexCol int
	var subsetCol, isInside, updateXgiIndex bool

	split := strings.Split(line, SEP)

	switch {
	case INDEXFIRSTCOL:
		xgi = split[0]
		split = split[1:]

		MUTEX2.Lock()
		if _, isInside = XGIMAP[xgi];isInside {
			if !RMDUPPLICATES {
				panic(fmt.Sprintf("Dupplicate index from first column: %s\n", xgi))
			} else {
				xgi = xgi + "_Duppl"
			}
		} else if XGISUBSET == "" {
			MUTEX.Lock()
			XGISUBSETMAP[xgi] = XDIM
			XGIMAP[xgi] = indexRow
			XGIINDEX = append(XGIINDEX, xgi)
			MUTEX.Unlock()
		}
		MUTEX2.Unlock()

	case XGI != "":
		xgi = XGIINDEX[indexRow]
	default:
		xgi = strconv.Itoa(indexRow)
		updateXgiIndex = true
	}

	if len(YGISUBSETMAP) > 0 {
		subsetCol = true
	}

	if len(XGISUBSETMAP) > 0 {
		MUTEX.Lock()
		if indexRow, isInside = XGISUBSETMAP[xgi];!isInside {
			guard <- true
			return
		}
		MUTEX.Unlock()
	}

	buffer := bytes.Buffer{}
	ydim := 0

	ygiIndexCreated := len(YGIINDEX) > 0

	if len(split) != len(YGIINDEX) {

		if len(YGIINDEX) > 0 {
			panic(fmt.Sprintf("#### Error: %d columns found for row nb %d. Inconsistent with the ygi index (%d columns) \n",
			len(split), indexRow, len(YGIINDEX)))

		}
	}

	for indexCol, valstr = range split {
		val, err = strconv.ParseFloat(valstr, 32)

		if ygiIndexCreated {
			ygi = YGIINDEX[indexCol]
		}

		if err != nil {
			panic(fmt.Sprintf("#### Error when trying to convert value: %s for col: %d and row :%d into float\n",
				valstr, indexCol, indexRow))
		}

		if subsetCol {
			if indexCol, isInside = YGISUBSETMAP[ygi];!isInside {
				continue
			}
		}

		ydim++

		if val == 0 {
			continue
		}

		index1 = strconv.Itoa(indexRow)
		index2 = strconv.Itoa(indexCol)

		if TRANSPOSE {
			index1, index2 = index2, index1
		}

		buffer.WriteString(index1)
		buffer.WriteString(OUTSEP)
		buffer.WriteString(index2)
		buffer.WriteString(OUTSEP)
		buffer.WriteString(valstr)
		buffer.WriteRune('\n')
	}

	MUTEX.Lock()

	XDIM++
	YDIM = ydim

	if !ygiIndexCreated {
		YGIINDEX = make([]string, len(split))

		for i:=0;i<len(split);i++ {
			YGIINDEX[i] = strconv.Itoa(i)
		}
	}

	if updateXgiIndex {
		XGIMAP[xgi] = indexRow
		XGIINDEX = append(XGIINDEX, xgi)
	} else {
		if indexRow >= len(XGIINDEX) {
			panic(fmt.Sprintf("#### Error: at the row nb: %d. Inconsistent with the xgi index (%d rows) \n",
				indexRow, len(XGIINDEX)))
		}
	}

	_, err = (*writer).Write(buffer.Bytes())
	utils.Check(err)

	MUTEX.Unlock()

	guard <- true
}

func readFirstLineAsHeader(reader *bufio.Scanner) {
	reader.Scan()
	utils.Check(reader.Err())
	firstLine := reader.Text()
	ygiindex := strings.Split(firstLine, SEP)

	if INDEXFIRSTCOL && !SHIFTHEADER {
		ygiindex = ygiindex[1:]
	}

	var ygiindexready []string

	indexClean := 0

	var isInside bool
	ygimap := make(map[string]int)

	for _, ygi := range ygiindex {
		if _, isInside = ygimap[ygi];isInside {
			if RMDUPPLICATES {
				ygi = ygi + "_Duppl"
			} else {
				panic(fmt.Sprintf("#### Error dupplicate column ID found in HEADER: %s\n", ygi))
			}
		} else {
			ygimap[ygi] = indexClean
			indexClean++
		}

		ygiindexready = append(ygiindexready, ygi)
	}

	YGIINDEX = ygiindexready

	if RMDUPPLICATES && len(YGISUBSETMAP) == 0 {
		YGISUBSETMAP = ygimap
	}
}

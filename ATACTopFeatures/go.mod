module gitlab.com/Grouumf/ATACdemultiplex/ATACTopFeatures

go 1.15

require (
	github.com/biogo/store v0.0.0-20201120204734-aad293a2328f
	github.com/glycerine/goconvey v0.0.0-20190410193231-58a59202ab31 // indirect
	github.com/glycerine/golang-fisher-exact v0.0.0-20160911222405-aea2106439d4
	github.com/glycerine/gostat v0.0.0-20160815084721-ccc4a6d847f9 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/skelterjohn/go.matrix v0.0.0-20130517144113-daa59528eefd // indirect
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210806184010-1697f664c91c
)

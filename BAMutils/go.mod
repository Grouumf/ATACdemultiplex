module gitlab.com/Grouumf/ATACdemultiplex/BAMutils

go 1.15

require (
	github.com/biogo/hts v1.2.2
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210802220835-4b20ff939e6b
)

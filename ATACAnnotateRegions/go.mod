module gitlab.com/Grouumf/ATACdemultiplex/ATACAnnotateRegions

go 1.15

require (
	github.com/biogo/store v0.0.0-20201120204734-aad293a2328f
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20230223181447-5849ca1afb1d
)

module gitlab.com/Grouumf/ATACdemultiplex/ATACMatUtils

go 1.15

require (
	github.com/biogo/store v0.0.0-20201120204734-aad293a2328f
	gitlab.com/Grouumf/ATACdemultiplex/ATACdemultiplexUtils v0.0.0-20210802220835-4b20ff939e6b
)
